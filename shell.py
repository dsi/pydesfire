#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Exécution d'un shell ipython avec initialisation de la connexion avec une carte

La variable card accessible depuis le shell continendra soit une instance de
DesFireEV1 quand le script est appellé sans argument, soit une instance de
DesFireCard si l'argument est un fichier de configuration.
"""

import sys

from IPython.terminal.embed import InteractiveShellEmbed
from IPython.lib.backgroundjobs import BackgroundJobManager
from IPython.terminal.ipapp import load_default_config
try:
    # ipython 5
    from IPython.terminal.prompts import Prompts, Token
except ImportError:
    Prompts = None

from smartcard.CardType import ATRCardType
from smartcard.CardRequest import CardRequest
from smartcard.Exceptions import CardRequestTimeoutException
from pydesfire.desfire import DesFireEV1
from pydesfire.card_config import CardConfig
from pydesfire.desfire_card import DesFireCard

import time

import logging
logging.basicConfig(
    format = "%(levelname)s - %(message)s",
    level = logging.DEBUG
)

# Import pratique pour le shell
from pydesfire.desfire_keys import DESFire_AES_key, DESFire_DES_key

if len(sys.argv) > 1:
    config = CardConfig(sys.argv[1])
    print('config={}("{}")'.format(config.__class__.__name__, sys.argv[1]))
else:
    config = None
    
def print_titre(msg):
    msg = ' ' + msg + ' '
    print('\n{:=^78}'.format(msg))

class CardShellObject(object):
    """
    Objet permettant d'accéder depuis le shell à l'instance de
    DesFireCard ou DesFireEV1 si il n'y a pas de config
    créé via le thread de surveillance du lecteur
    """
    def __init__(self, config):
        self.config = config
        self.card = None
        self.watch = False

    def __getattribute__(self, name):
        if name in ('card', 'config', 'watch', 'start_watch'):
            return object.__getattribute__(self, name)

        card = object.__getattribute__(self, 'card')
        if not card:
            return None
        else:
            return getattr(card, name)

    def __str__(self):
        card = object.__getattribute__(self, 'card')
        if not card:
            return 'No card'
        else:
            return str(card)

    def __dir__(self):
        card = object.__getattribute__(self, 'card')
        if not card:
            return []
        else:
            return dir(card)

    def start_watch(self):
        cardrequest = CardRequest(timeout=1, cardType=ATRCardType([ 0x3b, 0x81, 0x80, 0x01, 0x80, 0x80 ]))
        current_cards = []
        self.watch = True
        while self.watch:
            cards = cardrequest.waitforcardevent()
            if current_cards == cards:
                time.sleep(1)
                continue
            current_cards = cards
            if cards:
                print_titre('Initialisation carte')
                try:
                    conn = cards[0].createConnection()
                    conn.connect()

                    desfire = DesFireEV1(conn)

                    if config:
                        self.card = DesFireCard(config, desfire)
                        print('card={}(config, {}(cardservice.connection))'.format(card.__class__.__name__, desfire.__class__.__name__))
                    else:
                        self.card = desfire
                        print('card={}(cardservice.connection)'.format(desfire.__class__.__name__))

                except Exception:
                    logging.exception('Erreur start_watch')

            else:
                print_titre('Carte retirée')
                self.card = None
                conn.disconnect()
                
card = CardShellObject(config)
            
## Config IPython
cfg = load_default_config()
if Prompts:
    # ipython 5
    class DesFirePrompt(Prompts):
        def in_prompt_tokens(self, cli=None):
            return [
                (Token.Prompt, '%s [' % str(card)),
                (Token.PromptNum, str(self.shell.execution_count)),
                (Token.Prompt, ']: '),
            ]
    cfg.TerminalInteractiveShell.prompts_class = DesFirePrompt
else:
    # ipython 2
    prompt_config = cfg.PromptManager
    prompt_config.in_template = '{color.LightGreen}[{card}]{color.normal} <\\#>: '
    prompt_config.in2_template = '   .\\D.: '
    prompt_config.out_template = 'Out<\\#>: '

# Job de vérification de la précense d'une carte
job_manager = BackgroundJobManager()
job_manager.new(card.start_watch)

class Shell(InteractiveShellEmbed):
    def ask_exit(self):
        card.watch = False
        print('Arrêt application, patienter 2s')
        return super(InteractiveShellEmbed, self).ask_exit()
    
ipshell = Shell(config=cfg, banner1='')
ipshell()
