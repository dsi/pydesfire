#!/usr/bin/env python3
# -*- coding: utf-8 -*-

""" Test à réaliser avec une carte vierge 

Script rapide pour tests, pourrai être grandement
amélioré mais vraiement utile pour vérifier la non
régression

"""

from pydesfire.desfire import DesFireEV1
from pydesfire.desfire_keys import DESFire_AES_key, DESFire_DES_key
from pydesfire.utils import toBytes

from smartcard.CardType import ATRCardType
from smartcard.CardRequest import CardRequest

from pprint import pprint

master_key=DESFire_DES_key( '\x00' * 16 )  # il faudrait ajouter les tests de changement de clef maitre
    
cardtype = ATRCardType(toBytes("3B 81 80 01 80 80"))
cardrequest = CardRequest(timeout=1, cardType=cardtype)
cardservice = cardrequest.waitforcard()

cardservice.connection.connect()
card = DesFireEV1(cardservice.connection)

pprint(card.get_version())
print('Auth master')
card.auth(0,master_key)
pprint(card.get_key_settings())
print(card.get_card_uid())
print('Erase')
card.format_PICC()
print('Configure')
card.set_configuration()
card.set_default_key([1]*24, 255)
print('Version de clef', card.get_key_version(0))
card.change_key_settings( { 'free_create_delete' : True } )
card.create_application(123, 1)
assert card.get_application_ids() == [123]
card.delete_application(123)
assert card.get_application_ids() == []

for appid, crypto, key_class, ksize in ( (12345, 'AES',    DESFire_AES_key, 16),
                                         (12346, '2K3DES', DESFire_DES_key, 16),
                                         (12347, '3K3DES', DESFire_DES_key, 24)):

    print('=' * 80)
    print('TEST %s' % crypto)
    print('Create app')
    card.create_application(appid, 2, crypto)
    print('Select app')
    card.select_application(appid)
    print(card.get_key_version(0))
    print('Auth 0')
    k = key_class('\x01'*ksize)
    card.auth(0, k)
    k1 = key_class( card.get_random_bytes(ksize), card.get_random_bytes(1)[0] )
    print('Key Settings')
    pprint(card.get_key_settings())
    print("ChangeKey 1, version=%s" % k1.version)
    card.change_key(1, k1, k)
    print('GetKeyVersion')
    assert card.get_key_version(1) == k1.version
    k0 = key_class( card.get_random_bytes(ksize) )
    print("ChangeKey(0)")
    card.change_key(0, k0)
    print("Auth 0")
    card.auth(0, k0)
    for i, mode in ( (2, 'PLAIN'),
                     (1, 'MAC'),
                     (0, 'ENCRYPT')
                     ):
        data = card.get_random_bytes(100)
        print("Create %i mode %s" % (i, mode))
        card.create_datafile(i, mode, 1, 0, 0, 0, len(data))
        print("Write %i mode %s" % (i, mode))
        card.write_data(i, mode, data)
        print("Read %i mode %s" % (i, mode))
        rdata = card.read_data(i,mode)
        assert rdata == data

    print('Change Key Settings')
    card.change_key_settings({})
    print('Key Settings')
    pprint(card.get_key_settings())

    print("List files")
    for fid in card.get_file_ids():
        print(card.get_file_settings(fid))

    card.change_file_settings(0, comm_mode='PLAIN')
    print(card.get_file_settings(0))
    assert card.read_data(0, 'PLAIN') == data

    card.change_file_settings(0, comm_mode='MAC')
    print(card.get_file_settings(0))
    assert card.read_data(0, 'MAC') == data

    print('Delete app')
    card.delete_application(appid)
    
    print('Select 0')
    card.select_application(0)
    print('Auth master')
    card.auth(0,master_key)
    print('Free Mem', card.free_mem())
    
print('Erase')
card.format_PICC()
card.set_default_key([0]*24, 0)
