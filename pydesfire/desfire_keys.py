#! -*- coding:utf-8 -*-

"""
Objets des gestion des clefs DESFire.

© Frédéric Pauget <frederic.pauget@telecom-paristech.fr>

Ce logiciel est régi par la licence CeCILL, voir le fichier LICENSE.

"""

from .utils import toASCIIString, BinStringToHexList, toBytes
from Cryptodome.Cipher import AES, DES3, DES
from Cryptodome.Util.strxor import strxor

class DESFire_key:
    def __init__(self, value, version=None, use_diversification=False, comment=None):
        """
        @param value   : valeur de la clef
        @param version : version de la clef
        @param use_diversification : flag pour indiquer que la clef devrait être utilisée diversifiée
        @param comment : commentaire associé
        """
        self.iv = None
        self.version = version
        self.use_diversification = use_diversification
        self.comment = comment or ''
        if isinstance(value, str):
            self._Bytes = BinStringToHexList(value)
        else:
            self._Bytes = value

    @property
    def bsize(self):
        return self.base_cipher.block_size
            
    def __str__(self):
        if self.use_diversification == None:
            div = '+div'
        else:
            div = ''

        return 'key {}{} {} (v{})'.format(self.key_type, div,
                                          self.comment or ' '.join([ '{:02X}'.format(x) for x in self._Bytes ]),
                                          self.version)
    
    def __repr__(self):
        return '<{}>'.format(self.__str__())

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self._Bytes == other.Bytes
        else:
            return False

    def __len__(self):
        return len(self._Bytes)
    
    @property
    # Pour protection de la liste interne _Bytes
    def Bytes(self):
        """ Retourne la représentation sous forme de liste d'octets """
        if len(self._Bytes) == 8 :
            # Pour les clefs 8 octets on retourne le double, beurk :(
            return list(self._Bytes) * 2
        return list(self._Bytes)

    def xor(self, other):
        """ Effectue un xor logique avec other_key 
        Retourne une nouvelle clef
        """
        if len(self) < len(other):
            Bytes = list(self._Bytes * (len(other) // len(self)))
            if len(Bytes) < len(other):
                raise ValueError("bad key size")
        else:
            Bytes = list(self._Bytes)
        for i in range(len(Bytes)):
            Bytes[i] ^= other.Bytes[i]
        return Bytes
    
    @property
    def bin_str(self):
        """ Retourne la représentation sous forme de chaine binaire de la clef 
        ex : '"3DUfwª»ÌÝîÿ'
        """
        return toASCIIString(self._Bytes)
    
    def do_cypto(self, action, message, iv):
        if iv == None and self.iv:
            iv = self.iv
        if not iv:
            iv = b'\x00' * self.bsize

        return getattr(
            self.base_cipher.new(self.bin_str, self.base_cipher.MODE_CBC, iv),
            action)(toASCIIString(message))
    
    def encrypt(self, message, iv=None):
        """ Chiffrement du message fourni 
        @param message : liste d'octets du message a chiffrer
        @param iv : chaine binaire représentant vecteur d'initialisation cryptographique
        @return : listes d'octets du message chiffré
        """
        result = self.do_cypto('encrypt', message, iv)
        self.iv = result[-self.bsize:]
        return BinStringToHexList(result)

    def decrypt(self, message, iv=None):
        """ Déchiffrement du message fourni 
        @param message : liste d'octets du message a déchiffrer
        @param iv : chaine binaire représentant vecteur d'initialisation cryptographique
        @return : listes d'octets du message déchiffré
        """
        result = self.do_cypto('decrypt', message, iv)
        self.iv = toASCIIString(message[-self.bsize:])
        return BinStringToHexList(result)

    @property
    def subkey(self):
        size = len(self._Bytes)
        l = 0
        for i in range(size):
            l += self._Bytes[-1-i] * (2**(i*8))
        l = l << 1
        if self._Bytes[0] & 0x80 == 0x80:
            # MSB = 1
            l ^= { 16 : 0x87, 8 : 0x1b }[self.bsize]
            
        l &= 2**(8*size) - 1
        return self.__class__(toBytes('{{:0{}x}}'.format(2 * size).format(l)))

    def CMAC(self, message, iv=None, padsize=None):
        # padsize est nécessaire pour l'usage en diversification
        if padsize == None:
            padsize = self.bsize
        l = len(self._Bytes)
        pad = ( padsize - len(message) % padsize ) % padsize
        xor = self.__class__(BinStringToHexList(self.base_cipher.new(toASCIIString(self._Bytes), self.base_cipher.MODE_CBC, b'\x00' * self.bsize).encrypt(b'\x00' * self.bsize))).subkey
        if pad :
            message = message + [ 0x80 ] + [ 0 ] * (pad-1)
            xor = xor.subkey

        i = len(message) - self.bsize
        for j in range(self.bsize):
            message[i+j] ^= xor.Bytes[j]

        return self.encrypt(message, iv)[-self.bsize:]
    
class DESFire_DES_key(DESFire_key):
    """ Clef DES : 
    - soit DES 56 bits :
       si la longueur de la valeur est 8 octets
       ou si la longueur est 16 mais la clef symétrique
    - soit DES 112 bits si la longueur est 16 et asymétrique
    - soit DES 156 bits si la longeur des 24 octets

    >>> k = DESFire_DES_key(toBytes('00112233445566778899AABBCCDDEEFF'), 0)
    >>> k
    <key 2K3DES 00 10 22 32 44 54 66 76 88 98 AA BA CC DC EE FE (v0)>
    >>> DESFire_DES_key(k.encrypt([0] * 8, 0))
    <key DES FB 09 75 99 72 30 1A F4 (v240)>
    >>> k.divert(toBytes('04782E21801D803042F54E58502041'))
    <key 2K3DES+div 16 F8 58 7C 9E 88 10 C8 6A 96 48 D0 06 10 7C D6 (v0)>


    >>> k = DESFire_DES_key(toBytes('00112233445566778899AABBCCDDEEFF0102030405060708'))
    >>> k
    <key 3K3DES 00 11 22 33 44 55 66 77 88 99 AA BB CC DD EE FF 01 02 03 04 05 06 07 08 (v85)>
    >>> k.divert(toBytes('04782E21801D803042F54E5850'))
    <key 3K3DES+div 2E 0D D0 37 74 D3 FA 9B 56 05 AA 0B DA 91 CA 0B 54 B9 E0 7F CC BF 10 ED (v85)>
    """
    def __init__(self, value, version=None, use_diversification=False, comment=None):
        super(DESFire_DES_key, self).__init__(value, None, use_diversification, comment)

        if len(self._Bytes) == 8:
            self.key_type = 'DES'
            self.base_cipher = DES
        elif len(self._Bytes) in (16, 24) :
            # Type de clef présenti en fonction de la taille
            if len(self._Bytes) == 16 :
                self.key_type = '2K3DES'
            elif len(self._Bytes) == 24 :
                self.key_type = '3K3DES'

            # Cas patriculier où la clef dégénère en simple DES
            try:
                DES3.adjust_key_parity(self._Bytes)
            except ValueError:
                if len(self._Bytes) == 16 :
                    # Il faut traiter le cas 2K3DES dégénéré avec les instruction DES
                    self.key_type = 'DES'
                self.base_cipher = DES
                self._Bytes = self._Bytes[:8]
            else:
                self.base_cipher = DES3
        else:
            raise ValueError("Invalid Key length")

        # La version est stockée dans les bits de poids faible de la clef
        if version == None:
            # Calcul de la version
            self.version = 0
            for i in range(8):
                if self._Bytes[i] & 1:
                    self.version += 2**(7-i)

        else:
            self.version = version
            # Changement des bits de version en accord avec le choix
            for i in range(len(self._Bytes)):
                if (self.version >> (7-i%8)) & 1:
                    self._Bytes[i] |= 1
                else:
                    self._Bytes[i] &= 0xfe
                    
    def divert(self, diversifiant):
        """ Diversifiant : liste d'octets """
        import warnings
        warnings.warn('fonction expérimentale non testée')
        if self.key_type == '2K3DES':
            return self.__class__(self.CMAC([ 0x21 ] + diversifiant, 0) +
                                  self.CMAC([ 0x22 ] + diversifiant, 0),
                                  version = self.version,
                                  use_diversification = None,
                                  comment = self.comment)
        elif self.key_type == '3K3DES':
            return self.__class__(self.CMAC([ 0x31 ] + diversifiant, 0) +
                                  self.CMAC([ 0x32 ] + diversifiant, 0) +
                                  self.CMAC([ 0x33 ] + diversifiant, 0),
                                  version = self.version,
                                  use_diversification = None,
                                  comment = self.comment)
        raise NotImplementedError

    
class DESFire_AES_key(DESFire_key):
    """
    >>> k = DESFire_AES_key(toBytes('00112233445566778899AABBCCDDEEFF'))
    >>> DESFire_AES_key(k.encrypt([0] * 16, 0))
    <key AES FD E4 FB AE 4A 09 E0 20 EF F7 22 96 9F 83 83 2B (v0)>
    >>> k.divert(toBytes('04782E21801D803042F54E585020416275'))
    <key AES+div A8 DD 63 A3 B8 9D 54 B3 7C A8 02 47 3F DA 91 75 (v0)>
    """
    def __init__(self, value, version=0, use_diversification=False, comment=None):
        self.key_type = 'AES'
        self.base_cipher = AES
    
        super(DESFire_AES_key, self).__init__(value, version, use_diversification, comment)
        if len(self._Bytes) != 16:
            raise ValueError("Invalid Key length")

    def divert(self, diversifiant):
        """ Diversifiant : liste d'octets """
        return self.__class__(self.CMAC([ 0x01 ] + diversifiant, 0, 32),
                              version=self.version,
                              use_diversification=None,
                              comment=self.comment)

    

