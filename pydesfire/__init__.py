# -*- coding: utf-8 -*-

from .desfire import DesFireEV1
from .card_config import CardConfig
from .desfire_card import DesFireCard

def get_desfire_card(configfile_path, cardconnection):
    """ 
    Retourne une instance de DesFireCard à partir 
    du chemin d'un fichier de configuration et une connexion PCSC
    """
    desfire = DesFireEV1(cardconnection)
    config = CardConfig(configfile_path)
    return DesFireCard(config, desfire)
