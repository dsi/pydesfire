# -*- coding: utf-8 -*-

"""
Classe de gestion principale DesFireEV1 implémentant les opération de
communication avec la carte.

© Frédéric Pauget <frederic.pauget@telecom-paristech.fr>

Ce logiciel est régi par la licence CeCILL, voir le fichier LICENSE.

"""

from zlib import crc32

from .utils import toASCIIString, BinStringToHexList, toBytes

from Cryptodome import Random

## Fonctions utiles
def int_to_char_lsb(val, l):
    """ Conversion entier vers liste d'octets

    @param val : entier
    @param l   : taille de la liste
    @return    : liste d'octects, LSB en premier
    """
    ret = []
    for i in range(l):
        ret.append((val & (0xff << (i*8))) >> (i*8))
    return ret

def desfire_crc16(data):
    """ Calcul de crc suivant l'ISO ISO14443A

    @param data : liste d'octets
    @return     : liste d'octets

    """
    wCrc = 0x6363
    for v in data:
        ch = v ^ (wCrc & 0xff)
        ch = (ch ^ ((ch << 4) & 0xff)) & 0xff
        wCrc = (wCrc >>8) ^ (ch << 8) ^ (ch <<3) ^ (ch >> 4)

    crc = (((wCrc >> 8) & 0xff) << 8) | (wCrc & 0xff)
    return [crc & 0xff, (crc & 0xff00) >> 8]

def desfire_crc32(data):
    """ Calcul CRC32  pour opérations desfire

    @param data : liste d'octets
    @return     : liste d'octets
    """
    crc = crc32(toASCIIString(data)) & 0xffffffff ^ 0xffffffff
    if crc < 0:
        crc = (-1 * (crc^0xffffffff)) - 1
    return [crc & 0xff,
            (crc & 0xff00) >> 8,
            (crc & 0xff0000) >> 16,
            (crc & 0xff000000) >> 24
           ]

class CardError(Exception):
    pass

class CardAuthenticationError(CardError):
    pass

class CardApplicationNotFound(CardError):
    pass

class DesFireEV1(object):
    crypto_types = {'AES' : 0x80, '3K3DES' : 0x40, '2K3DES' : 0x00, 'DES': 0x00 }
    error_codes = {
        0xa1 : "APPL_INTEGRITY_ERROR  : Unrecoverable error within application, application will be disabled",
        0xc1 : "PICC_INTEGRITY_ERROR  : Unrecoverable error within PICC, PICC will be disabled",
        0xcd : "PICC_DISABLED_ERROR : PICC  was disabled by an unrecoverable error",
        0xee : "EEPROM_ERROR : Could not complete NV-write operation due to loss of power, internal backup/rollback mechanism activated",
        0xf1 : "FILE_INTEGRITY_ERROR : Unrecoverable error within file, file will be disabled",
        0x1c : "ILLEGAL_COMMAND_CODE : Command code not supported",
        0x1e : "INTEGRITY_ERROR : CRC or MAC does not match data or Padding bytes not valid",
        0x40 : "NO_SUCH_KEY : Invalid key number specified",
        0x7e : "LENGTH_ERROR : Length of command string invalid",
        0x9e : "PARAMETER_ERROR : Value of the parameter(s) invalid",
        0xbe : "BOUNDARY_ERROR : Attempt to read/write data from/to beyond the file's/record's limits",
        0xca : "COMMAND_ABORTED : Previous Command was not fully completed or Not all Frames were requested or provided by the PCD",
        0xce : "COUNT_ERROR : Number of Applications limited to 28, no additional CreateApplication possible",
        0xde : "DUPLICATE_ERROR : Creation of file/application failed because file/application with same number already exists",
        0xf0 : "FILE_NOT_FOUND : Specified file number does not exist",
        0x0c : "NO_CHANGES : No changes done to backup files, CommitTransaction / AbortTransaction not necessary",
        0x0e : "OUT_OF_EEPROM_ERROR : Insufficient NV-Memory to complete command",
        0x9d : "PERMISSION_DENIED : Current configuration / status does not allow the requested command",
        0xae : "AUTHENTICATION_ERROR : Current authentication status does not allow the requested command",
    }
    
    def __init__(self, cardconnection):
        self.connection = cardconnection

        self.current_app = 0
        self.session_key = None
        self.auth_cmd = None

        self.random = Random.new()

    def __str__(self):
        return '{} card'.format(self.__class__.__name__)
        
    def get_random_bytes(self, n):
        return BinStringToHexList(self.random.read(n))

    def send_iso7816_cmd(self, cla, ins, param1=0, param2=0, data=[], le=0):
        """ Envoi d'APDU au format ISO 7816
        cla	1	      Classe d'instruction
        ins	1 	      Code d'instruction
        param	2	      Paramètres d'instructions
        data	Nc	      Nc octets
        le	0, 1, 2 ou 3  Nombre (Ne) maximum d'octets attendus dans la réponse
        """
        apdu = [cla, ins, param1, param2]

        # Ajout a l'apdu : len(data), data, le
        if data:
            lc = len(data)
            if lc > 255:
                apdu.append(0xff)
                apdu.append(lc - 255)
            else:
                apdu.append(lc)
            apdu += data
        apdu.append(le)
        try:
            read, sw1, sw2 = self.connection.transmit(apdu)
        except Exception as exception:
            self.session_key = None
            raise exception
        
        if sw1 != 0x91:
            self.session_key = None
            raise CardError('UNKNOWN ERROR')
            
        if sw2 not in (0x00, 0xaf):
            if sw2 == 0xae:
                raise CardAuthenticationError("AUTHENTICATION_ERROR : Current authentication status does not allow the requested command")
            if sw2 == 0xa0:
                raise CardApplicationNotFound("APPLICATION_NOT_FOUND : Requested AID not present on PICC")
            self.session_key = None
            raise CardError(self.error_codes.get(sw2, 'UNKNOWN DESFIRE ERROR'))

        return read, sw1, sw2
        
    def send_desfire_cmd(self, cmd, data=[], cmac=False, cont=True):
        """ Envoi d'une commande DESFire encapsulée ISO 7816
        @param cmd  : commande DESFire
        @param data : données à transmette
        @param cmac : indique si la commande attend une réponse avec CMAC
        @param cont : en cas de code de réponse 0xaf renvoie 0xaf
                      et aggrège les données reçues

        @return     : retour de la carte
        """
        result = []
        read, sw1, sw2 = self.send_iso7816_cmd(0x90, cmd, data=data[:54])
        data = data[54:]
        result += read

        while sw2 == 0xaf and (data or cont):
            read, sw1, sw2 = self.send_iso7816_cmd(0x90, 0xaf, data=data[:54])
            data = data[54:]
            result += read

        if self.session_key and cmac:
            cmac = self.session_key.CMAC(result[:-8] + [sw2])
            assert cmac[:8] == result[-8:], CardError("CMAC error")
            result = result[:-8]

        return result

    def send_desfire_cmac_cmd(self, cmd, data=[], cmac=True, send_cmac=False):
        """ Envoi d'une commande à la carte avec vérification CMAC
        @param cmd  : commande DESFire
        @param data : données à transmette
        @param cmac : indique si la commande attend une réponse avec CMAC

        @return     : retour de la carte
        """
        if self.session_key:
            mac = self.session_key.CMAC([cmd] + data)
            if send_cmac:
                data = data + mac[:8]
        return self.send_desfire_cmd(cmd, data, cmac)

    def send_desfire_encrypted_cmd(self, cmd, args, data, cmac=True):
        """ Envoi d'une commande à la carte avec données chiffrées
        @param cmd  : commande DESFire
        @param args : arguments de la commandes (transmis en clair)
        @param data : données à transmette chiffrées
        @param cmac : indique si la commande attend une réponse avec CMAC

        @return     : retour de la carte
        """
        if not self.session_key:
            raise ValueError("Must be authenticated")

        while len(data) % self.session_key.bsize != 0:
            data.append(0)

        edata = self.session_key.encrypt(data)
        return self.send_desfire_cmd(cmd, args + edata, cmac)

    def send_desfire_encrypted_crc_cmd(self, cmd, args, data):
        """ Envoi d'une commande à la carte avec données chiffrées et calcul de CRC
        @param cmd  : commande DESFire
        @param args : arguments de la commandes (transmis en clair)
        @param data : données à transmette chiffrées

        @return     : retour de la carte
        """
        if not self.session_key:
            raise ValueError("Must be authenticated")

        if self.auth_cmd == 0x0a:
            clear = data + desfire_crc16(data)

        elif self.auth_cmd in (0x1a, 0xaa):
            clear = data + desfire_crc32([cmd] + args + data)

        return self.send_desfire_encrypted_cmd(cmd, args, clear)

    ##############################  ##############################
    def parse_version(self, data):
        # la version (majeure, mineure) est en BCD
        return {'vendor'  : data[0],
                'type'    : data[1],
                'subtype' : data[2],
                'version' : '{:x}.{:x}'.format(data[3], data[4]),
                'size'    : 2**(data[5] >> 1),
                'protocol' : data[6],
               }

    def get_version(self):
        """ Récupération de la version de la carte et de son UID """
        data = self.send_desfire_cmac_cmd(0x60)
        card_version = {
            'hardware' : self.parse_version(data),
            'software' : self.parse_version(data[7:]),
            'UID' : data[14:21],
            'batch_number' : data[21:26],
            'week' : int('{:02x}'.format(data[26])),
            'year' : int('20{:02x}'.format(data[27]))
        }
        if card_version['UID'] == [0, 0, 0, 0, 0, 0, 0]:
            card_version['UID'] = None

        return card_version

    def get_card_uid(self):
        """ Récupération du CSN de la carte
        nécessite une authentification valide (n'importe quelle clef ou app)

        @return : CSN
        """
        # en 2 ligne afin d'avoir la bonne exception
        edata = self.send_desfire_cmac_cmd(0x51, [], False)
        data = self.session_key.decrypt(edata)
        value = data[:7]
        
        # Verif CRC
        crc1 = desfire_crc32(value + [0])
        crc2 = data[7:11]
        assert crc1 == crc2, CardError("CRC error {}!={}".format(crc1, crc2))

        return value

    def free_mem(self):
        """ Information sur l'espace libre

        @return : octets libres
        """
        data = self.send_desfire_cmac_cmd(0x6e)
        if len(data) != 3:
            # soucis
            raise NotImplementedError

        return data[2] * 256 * 256 + data[1] * 256 + data[0]

    def set_configuration(self, disable_format=False, enable_randId=False):
        """ Configuration de la carte

        ATTENTION : commandes irréversibles !

        @param enable_format : supprimer la commande format
        @param enable_randId : configurer la carte en random UUID
        """
        val = 0x00
        if disable_format:
            val |= 0x01
        if enable_randId:
            val |= 0x02

        return self.send_desfire_encrypted_crc_cmd(0x5c, [0x00], [val])

    def set_default_key(self, key_bytes, version):
        """ Défini la clef par défaut pour les nouvelles applications

        @param key_bytes : valeur de la clef par défaut (24 octets)
        @param version   : version par défaut
        """
        while len(key_bytes) < 24:
            key_bytes.append(0)
        return self.send_desfire_encrypted_crc_cmd(0x5c, [0x01], key_bytes + [version])

    ############################## Commandes d'authentification ##############################
    def auth(self, keyno, key):
        """ Authentification desfire, mode standard

        @param keyno : numéro de clef
        @param key   : clef à utiliser instance de DESFIRE_key
        """
        self.auth_cmd = {'DES' : 0x1a, '2K3DES' : 0x1a, '3K3DES' : 0x1a, 'AES' : 0xaa}[key.key_type]
        # Recup enc(RndB)
        eRndB = self.send_desfire_cmd(self.auth_cmd, [keyno], cont=False)
        RndB = key.decrypt(eRndB, 0)

        # Génération RndA et envoi de dec(RndA + RndB'), la carte doit renvoyer RndA'
        RndA = self.get_random_bytes(len(eRndB))
        eRndA = self.send_desfire_cmd(0xaf, key.encrypt(RndA + RndB[1:] + RndB[:1]), cont=False)
        assert key.decrypt(eRndA) == RndA[1:] + RndA[:1], CardError("RndA error")

        if key.key_type == 'AES':
            self.session_key = key.__class__(RndA[:4] + RndB[:4] + RndA[12:] + RndB[12:])
        elif key.key_type == 'DES':
            self.session_key = key.__class__(RndA[:4] + RndB[:4])
        elif key.key_type == '2K3DES':
            self.session_key = key.__class__(RndA[:4] + RndB[:4] + RndA[4:] + RndB[4:])
        elif key.key_type == '3K3DES':
            self.session_key = key.__class__(RndA[:4] + RndB[:4] + RndA[6:10] + RndB[6:10] + RndA[12:] + RndB[12:])

    def change_key(self, keyno, newkey, oldkey=None):
        """ Changement de clef

        Avant l'utilisation de cette commande il faut être authentifié
        auprès de l'application courrante.

        L'ancienne clef doit être uniquement et obligatoirement fournie
        si la clef changée n'est pas celle utilisée pour l'authentification

        @param keyno   : numéro de clef
        @param newkey  : nouvelle clef
        @param oldkey  : ancienne clef
        """
        AES = newkey.key_type == 'AES'
        if self.current_app == 0 and keyno == 0:
            # Cas particulier master Key
            if newkey.key_type == '3K3DES':
                keyno = 64
            elif AES:
                keyno = 128

        if self.auth_cmd == 0x0a:
            if oldkey:
                data = oldkey.xor(newkey)
                if AES:
                    data.append(newkey.version)
                data += desfire_crc16(data) + desfire_crc16(newkey.Bytes)
            else:
                data = newkey.Bytes
                if AES:
                    data.append(newkey.version)
                data = data + desfire_crc16(data)

        elif self.auth_cmd in (0x1a, 0xaa):
            if oldkey:
                data = oldkey.xor(newkey)
                if AES:
                    data.append(newkey.version)
                data += desfire_crc32([0xc4, keyno] + data)
                data += desfire_crc32(newkey.Bytes)

            else:
                data = newkey.Bytes
                if AES:
                    data.append(newkey.version)
                data += desfire_crc32([0xc4, keyno] + data)

        else:
            raise RuntimeError("Not authenticated")

        self.send_desfire_encrypted_cmd(0xc4, [keyno], data, cmac=bool(oldkey))
        if not oldkey:
            self.session_key = None

    def get_key_version(self, keyno):
        """ Version de la clef

        @param keyno : numéro de clef
        @return      : version de la clef
        """
        return self.send_desfire_cmac_cmd(0x64, [keyno])[0]

    def get_key_settings(self):
        """ Information sur les clefs

        @return : dictionnaire des options (cf change_key_settings)
        """
        data = self.send_desfire_cmac_cmd(0x45)
        settings = {'master_changeable'  : bool(data[0] & 0b1),
                    'free_list'          : bool(data[0] & 0b10),
                    'free_create_delete' : bool(data[0] & 0b100),
                    'config_changeable'  : bool(data[0] & 0b1000),
                   }

        if self.current_app != 0:
            if data[1] & 0x80 == 0x80:
                settings['key_type'] = 'AES'
            elif data[1] & 0x40 == 0x40:
                settings['key_type'] = '3K3DES'
            else:
                settings['key_type'] = 'DES'
            
            settings['change_key_no'] = (data[0] & 0xf0) >> 4
            settings['nb_keys'] = data[1] & 0x0f
            
        return settings

    def change_key_settings(self, new_settings):
        """ Changement du paramétrage des clefs l'application courrante ou de la carte 

        @param new_settings : dictionnaire des options à changer clefs du dictionnaire :
         master_changeable  : autorise le changement de la clef maitre
         free_list          : autorise le listing sans authentification avec la clef maitre
         free_create_delete : autorise la création d'application/fichier et la destruction
                              de fichier sans authentification avec la clef maitre
         config_changeable  : autorise les changements ultérieurs de configuration
         change_key_no      : 0..13 numero de la clef permettant le changement de clef pour l'application courrante
         (unquement pour app)    14 changement de clef après authentification par la clef devant être changée
                                 15 changement de clef interdits sauf clef maitre
        """
        orig_settings = self.get_key_settings()
        settings = 0
        if new_settings.get('master_changeable', orig_settings['master_changeable']):
            settings |= 0b1
        if new_settings.get('free_list', orig_settings['free_list']):
            settings |= 0b10
        if new_settings.get('free_create_delete', orig_settings['free_create_delete']):
            settings |= 0b100
        if new_settings.get('config_changeable', orig_settings['config_changeable']):
            settings |= 0b1000
        if self.current_app != 0 and new_settings.get('change_key_no', orig_settings['change_key_no']):
            settings |= new_settings['change_key_no'] << 4

        self.send_desfire_encrypted_crc_cmd(0x54, [], [settings])

    ############################## Commandes de gestion des applications  ##############################
    def create_application(self, app_id, nbkeys, crypto='AES'):
        """ Création d'une application

        @param app_id : id de l'application (numérique)
        @param nbkeys : nombre de clefs de l'application
        @param crypto : type de chiffrement (clef de crypto_types)
        """
        data = toBytes('{:06x}'.format(app_id))
        data.reverse()
        data.append(0x0b) # default settings
        data.append(nbkeys & 0x0f | self.crypto_types[crypto])
        self.send_desfire_cmac_cmd(0xca, data)

    def delete_application(self, app_id):
        """ Supression d'une application 

        @param app_id : id de l'application (numérique)
        """
        data = toBytes('{:06x}'.format(app_id))
        data.reverse()
        if self.current_app == app_id:
            # Perte de l'authentification
            self.session_key = None
        self.send_desfire_cmac_cmd(0xda, data)

    def get_application_ids(self):
        """ Listes les id d'application de la carte """
        data = self.send_desfire_cmac_cmd(0x6a)

        aids = []
        for i in range(len(data)//3):
            aids.append(data[3*i] + data[3*i+1] * 256 + data[3*i+2] * 65536)

        return aids

    def select_application(self, app_id):
        """ Sélection de l'application courrante

        @param app_id : id de l'application (numérique)
        """
        # perte de l'authentification
        self.session_key = None

        data = toBytes('{:06x}'.format(app_id))
        data.reverse()
        self.send_desfire_cmd(0x5a, data)

        self.current_app = app_id

    def format_PICC(self):
        """ Effacement de toute les applications avec récupération de l'espace """
        if self.current_app != 0:
            self.session_key = None
        self.send_desfire_cmac_cmd(0xfc)

    ############################## Commandes de gestion des fichiers  ##############################
    def get_file_ids(self):
        """ Retourne les id de fichiers de l'application courrante """
        return self.send_desfire_cmac_cmd(0x6f)

    def get_file_settings(self, fileno):
        """ Retourne le paramétrage du fichier

        @param fileno : numéro de fichier
        @return       : dictionnaire des paramètres (cf change_file_settings)
        """
        settings = self.send_desfire_cmac_cmd(0xf5, [fileno])
        resp = {
            'ftype'     : ['std', 'backup', 'value', 'linear_record', 'cyclic_record'][settings[0]],
            'comm_mode' : {0 : 'PLAIN', 1: 'MAC', 3: 'ENCRYPT'}.get(settings[1]),
            'r_key'     : (settings[3] & 0xf0) >>4,
            'w_key'     : settings[3] & 0xf,
            'rw_key'    : (settings[2] & 0xf0) >>4,
            'm_key'     : settings[2] & 0xf
        }

        if settings[0] <= 1:
            resp['size'] = settings[4] | (settings[5] << 8) | (settings[6] << 16)
        elif settings[0] == 2:
            resp['min'] =  settings[4] |  (settings[5] << 8) |  (settings[6] << 16) |   (settings[7] << 24)
            resp['max'] =  settings[8] |  (settings[9] << 8) | (settings[10] << 16) |  (settings[11] << 24)
            resp['limited_credit_value'] = settings[12] | (settings[13] << 8) | (settings[14] << 16) |  (settings[15] << 24)
            resp['limited_credit'] = bool(settings[16])
        else:
            resp['size'] = settings[4]  | (settings[5] << 8) |  (settings[6] << 16)
            resp['nb']  = settings[7]  | (settings[8] << 8) |  (settings[9] << 16)
            resp['current']   = settings[10] | (settings[11] << 8) | (settings[12] << 16)

        return resp

    def change_file_settings(self, fileno, **new_settings):
        """ Changement des droits d'accès du fichier 

        @param fileno       : numéro du fichier
        @param new_settings : dictionnaire des nouveaux paramètres :
            comm_mode : mode de communication : PLAIN, MAC, CRYPT
            r_key   : n⁰ clef lecture / getvalue & débit
            w_key   : n⁰ clef écriture / getvalue & débit & limit crédit
            rw_key  : n⁰ clef lecture&écriture / getvalue & débit & limit crédit & crédit
            m_key   : n⁰ clef changement de droits

        n⁰ clef :
         * {0..13} désigne une clef (la clef doit exister)
         * 14 : accès libre
         * 15 : accès interdit
        """
        settings = self.get_file_settings(fileno)
        settings.update(new_settings)
        self.send_desfire_encrypted_crc_cmd(
            0x5f, [fileno],
            [{'PLAIN' : 0, 'MAC': 1, 'ENCRYPT' : 3}[settings['comm_mode']],
             settings['rw_key'] << 4 + settings['m_key'],
             settings['r_key'] << 4 + settings['w_key']])

    def delete_file(self, fileno):
        """ Destruction d'un fichier 

        @param fileno : numéro de fichier
        """
        self.send_desfire_cmac_cmd(0xdf, [fileno])
        
    def create_file(self, fileno, ftype, comm_mode, r_key, w_key, rw_key, m_key, **kwargs):
        """ Création d'un fichier
        
        @param ftype : type de fichier :
           * std
           * backup
           * value
           * linear_record
           * cyclic_record
        @param fileno : numéro de fichier
        @params comm_mode, r_key, w_key, rw_key, m_key : paramètres d'accès (cf change_file_settings)
        @param kwargs en fonction du type de fichier
           size : taille du fichier (std, backup) ou de l'enregistrement (*_record),
           nb : nombre maximal d'enregistrements (*_record)
           min, max, value : valeurs mini, maxi et initiale (value)
           limited_credit : True|False (value)
        """
        args = [ fileno,
                 {'PLAIN' : 0, 'MAC': 1, 'ENCRYPT' : 3}.get(comm_mode),
                 rw_key << 4 + m_key, r_key << 4 + w_key
        ]
        cmd = { 'std'    : 0xcd,
                'backup' : 0xcb,
                'value'  : 0xcc,
                'linear_record' : 0xc1,
                'cyclic_record' : 0xc0
        }.get(ftype, None)
        if not cmd:
            raise ValueError('Bad ftype')
        
        if ftype == 'value':
            args += int_to_char_lsb(kwargs['min'], 4)
            args += int_to_char_lsb(kwargs['max'], 4)
            args += int_to_char_lsb(kwargs['value'], 4)
            args += [ kwargs['limited_credit'] and 1 or 0 ]
        else:
            args += int_to_char_lsb(kwargs['size'], 3)
            if 'record' in ftype:
                args += int_to_char_lsb(kwargs['nb'], 3)
            
        self.send_desfire_cmac_cmd(cmd, args)
            

    def create_datafile(self, fileno, comm_mode, r_key, w_key, rw_key, m_key, size, backup=False):
        """ Création d'un standard datafile ou backup data file
        cf doc de create_file
        """
        self.create_file(fileno, backup and 'backup' or 'std',
                         comm_mode, r_key, w_key, rw_key, m_key,
                         size=size)
            
    def read_data(self, fileno, comm_mode, offset=0, length=0, record_size=None):
        """ Lecture du contenu d'un fichier

        @param fileno : numéro de fichier
        @param comm_mode : mode de communication : PLAIN, MAC ou ENCRYPT
        @param offset : offset de début de lecture
        @param length : taille à lire (0 pour tout le fichier)

        @return       : liste d'octets lus
        """
        args = [fileno] + int_to_char_lsb(offset, 3) + int_to_char_lsb(length, 3)

        if record_size:
            cmd = 0xbb
        else:
            cmd = 0xbd

        if comm_mode in ('PLAIN', 'MAC'):
            value = self.send_desfire_cmac_cmd(cmd, args)

        else:
            edata = self.send_desfire_cmac_cmd(cmd, args, False)
            data = self.session_key.decrypt(edata)

            # Calcul de la longueur
            if length == 0:
                length = len(data) - 1
                for char in reversed(data):
                    if char == 0:
                        length += -1
                    else:
                        break
                assert data[length] == 0x80, CardError("Incorrect FLT result")
                length += -4

            value = data[:length]

            # Verif CRC
            crc1 = desfire_crc32(value + [0])
            crc2 = data[length:length+4]
            assert crc1 == crc2, CardError("CRC error {}!={}".format(crc1, crc2))

        if record_size:
            result = []
            for i in range(0, len(value), record_size):
                result.append(value[i:i+record_size])
            return result
        else:
            return value

    def write_data(self, fileno, comm_mode, data, offset=0):
        """ Ecriture dans un fichier

        @param fileno : numéro de fichier
        @param comm_mode : mode de communication : PLAIN, MAC ou ENCRYPT
        @param data  : liste d'octets à écrire
        @param offset : offset de début d'écriture
        """
        length = len(data)
        cmd = [fileno] + int_to_char_lsb(offset, 3) + int_to_char_lsb(length, 3)

        if comm_mode == 'PLAIN':
            self.send_desfire_cmac_cmd(0x3d, cmd + data)
        elif comm_mode == 'MAC':
            self.send_desfire_cmac_cmd(0x3d, cmd + data, send_cmac=True)
        elif comm_mode == 'ENCRYPT':
            self.send_desfire_encrypted_crc_cmd(0x3d, cmd, data)
        else:
            raise ValueError("Bad comm_mode")
