# -*- coding: utf-8 -*-

"""
Classe de gestion de la configuration et d'écriture des données sur une carte DesFire EV1

© Frédéric Pauget <frederic.pauget@telecom-paristech.fr>

Ce logiciel est régi par la licence CeCILL, voir le fichier LICENSE.
"""

from __future__ import unicode_literals

from .utils import toBytes
from .desfire import CardError, CardAuthenticationError, CardApplicationNotFound

import logging

class DivertError(Exception):
    """ Diversification non utilisable """
    pass

class AuthError(Exception):
    """ Echec d'authentification """
    pass

class ConfigError(Exception):
    """ Configuration du fichier non valide """
    pass

class UsageError(Exception):
    """ Utilisation non possible dans le contexte """
    pass

class DesFireCard(object):
    """
    Les principales méthodes d'interface de haut niveau sont :

    * read_all
    * read_file
    * write_file
    * format_PICC
    * update_card
    """
    def __init__(self, config, card):
        """
        @param config : instance de CardConfig
        @param card   : instance de DesFireEV1
        """
        self.log = logging.getLogger('ENSTcard')
        self.config = config
        self.card = card

        # Données de la carte
        self.card_uid = None
        self.card_keys = { 0 : {} }
        self.cur_configs = {}
        self.files = {}
        
        # Etat courrant
        self.current_app = 0
        self.auth_key_id = None

    def __str__(self):
       return '{} app {} auth {}'.format(self.__class__.__name__, self.current_app, self.auth_key_id)

    def check_desfire_call(self, method, *args, **kwargs):
        """ Wrapper autour de self.card afin d'intercepter les
        erreur et changer le statut d'authenfication en
        fonction de celle-ci.
        """
        try:
            return getattr(self.card, method)(*args, **kwargs)
        except CardError:
            self.auth_key_id = None
            raise
        
    def get_uid(self):
        """ Récupération de l'UID de la carte et stockage pour usage ultérieur

        Retourne None en cas de non possiblité de lecture
        """
        if self.card_uid:
            return self.card_uid
        
        version = self.check_desfire_call('get_version')
        self.card_uid = version['UID']
        if self.card_uid:
            self.random_uid = False
            self.log.debug("UID %s (mode random UID non actif)", self.card_uid)
            return self.card_uid

        self.random_uid = True
        try:
            self.card_uid = self.check_desfire_call('get_card_uid')
            self.log.debug("UID %s (mode random UID actif)", self.card_uid)
        except CardAuthenticationError:
            self.log.debug("UID non lu (mode random UID actif)")

        return self.card_uid
            
    ### Gestion des applications
    def select_application(self,app_id):
        """ Sélection de l'application courrante """
        if self.current_app == app_id:
            return
        self.auth_key_id = None
        try:
            self.card.select_application(app_id)
        except CardApplicationNotFound as e:
            self.card.select_application(self.current_app)
            raise e
        self.current_app = app_id
        if self.current_app not in self.card_keys:
            self.card_keys[self.current_app] = {}

    def create_application(self, app_id):
        """ Création d'une application conformément à la configuration

        @param app_id : identifiant de l'application
        """
        nb_keys = self.config.get_setting(app_id, 'nb_keys')
        key_type = self.config.get_setting(app_id, 'key_type')
        if not nb_keys or not key_type:
            self.log.warn("app %s : création impossible, mauvais paramètres nb_keys=%s key_type=%s", app_id, nb_keys, key_type)
            raise ConfigError
        
        self.log.info("app %s : création, %i clefs %s", app_id, nb_keys, key_type)
        self.select_application(0)

        if not self.get_configuration('free_create_delete', True):
            self.auth(0)

        self.check_desfire_call('create_application', app_id, nb_keys, key_type)
        self.select_application(app_id)
        
        # Initialisation des dictionnaires d'état
        for key_id in range(self.config.get_setting(app_id, 'nb_keys')):
            self.card_keys[self.current_app][key_id] = self.config.get_default_key(key_type)

    ### Authenfification, gestion des clefs
    def auth(self, key_id, key=None):
        """
        Authentification auprès de la carte, stocke la clef utilisée
        pour une éventuelle réutilisation.

        La valeur stockée sera toujours préférée à la valeur fournie
        pour une réauthentification.

        @param key_id : numéro de la clef à utiliser
        @param key : valeur de la clef à utiliser,
                 si None prend la valeur du fichier de configuration
                 correspondant à la version ou la valeur par défaut
        """
        if self.auth_key_id == key_id:
            return

        if self.current_app == 0 and key_id != 0:
            raise UsageError
        
        prev_key = self.card_keys[self.current_app].get(key_id)
        if prev_key == False and not key:
            # Déjà tenté
            raise AuthError('app %i clef %i : aucune clef valide' % (self.current_app, key_id))
        
        if prev_key:
            key = prev_key

        if not key:
            try:
                version = self.check_desfire_call('get_key_version', key_id)
            except CardError:
                # version inconnue : pas grave, on teste tout
                version = None
            keys = self.config.get_keys(self.current_app, key_id)
            # Tri des clefs par version puis par ordre d'apparition inverse dans le fichier
            keyset1 = [ key for key in keys if key.version == version ]
            keyset1.reverse()
            keyset2 = [ key for key in keys if key.version != version ]
            keyset2.reverse()
            for key in keyset1 + keyset2:
                try:
                    return self.auth(key_id, key)
                except (AuthError, DivertError):
                    continue
            raise AuthError

        if key.use_diversification:
            key = self.divert_key(key)
        
        self.auth_key_id = None
        try:
            self.card.auth(key_id, key)
            self.auth_key_id = key_id
            result = 'OK'
            self.card_keys[self.current_app][key_id] = key
        except CardAuthenticationError:
            result = 'KO'
            if key_id in self.card_keys[self.current_app]:
                self.card_keys[self.current_app][key_id] = False

        self.log.debug('app %i clef %i : auth %s %s', self.current_app, key_id, result, key)

        if result != 'OK':
            raise AuthError

        # Au cas où pour récupérer l'UID
        self.get_uid()

    def auth_select(self, key_ids):
        """
        Authentification auprès de la carte avec un des id de clef fourni

        @param key_ids : liste d'id de clefs
        """
        if self.auth_key_id in key_ids:
            return
        
        for key_id in key_ids:
            if key_id in self.card_keys[self.current_app] and self.card_keys[self.current_app][key_id]:
                return self.auth(key_id)
                
        for key_id in key_ids:
            try:
                return self.auth(key_id)
            except AuthError:
                continue

        raise AuthError

    def divert_key(self, key):
        """ Diversification de la clef fournie
        
        Retourne DivertError si la diversification est impossible
        (UID de la carte non lisible)

        @param key : clef (instance de DESFire_key) à diversifier
        """
        if not self.card_uid:
            self.get_uid()
            
        if self.card_uid:
            return key.divert( self.card_uid + self.card_uid + [ 128 ])
        else:
            raise DivertError

    def update_keys(self):
        """ Mise à jour de toutes les clefs de l'application courrante """
        # Authentification avec la clef permettant les modifications de clef
        modkey = self.get_configuration('change_key_no', 0)
        self.auth(modkey)

        key_ids = self.config.list_keys(self.current_app)
        # Clef de modif en dernier
        if modkey in key_ids:
            key_ids.append(key_ids.pop(modkey))
            
        for key_id in key_ids:
            new_key = self.config.get_keys(self.current_app, key_id)[-1]
            old_key = self.card_keys[self.current_app][key_id]
            if new_key.use_diversification:
                real_key = self.divert_key(new_key)
            else:
                real_key = new_key
            if real_key == old_key:
                continue
            self.log.info("app %s clef %i : MAJ vers %s", self.current_app, key_id, real_key)
            if key_id == self.auth_key_id:
                self.auth_key_id = None
                self.card.change_key(key_id, real_key)
            else:
                self.check_desfire_call('change_key', key_id, real_key, old_key)
                
            self.card_keys[self.current_app][key_id] = new_key

    ### Gestion de la configuration
    def get_configuration(self, item, default=None):
        """ Retourne la configuration de l'application courrante
        
        Si il est impossible d'obtenir les données de depuis la carte
        les données du fichier de configuration sont retournées.

        @param item    : item de configuration à retourner
                         si None retourne un dictionnaire avec tous les items
        @param default : valeur par défaut si l'item est non trouvé
        """
        conf = self.cur_configs.get(self.current_app)
        if not conf:
            try:
                self.auth(0)
            except AuthError:
                return self.config.get_setting(self.current_app, item, default)
            else:
                conf = self.cur_configs[self.current_app] = self.check_desfire_call('get_key_settings')
                
        return conf.get(item, default)
    
    def update_configuration(self):
        """ Mise à jour de la configuration de l'application courrante """
        conf_mods = {}
        for key in ( 'master_changeable', 'free_list', 'free_create_delete', 'config_changeable', 'change_key_no' ):
            cur_value = self.get_configuration(key)
            conf_value = self.config.get_setting(self.current_app, key)
            if conf_value == None:
                continue
            if conf_value != cur_value:
                self.log.info('app %i config %s à changer : %s → %s', self.current_app, key, cur_value, conf_value)
                conf_mods[key] = conf_value

        if not conf_mods:
            self.log.debug('app %s config OK', self.current_app)
        elif self.current_app != 0 and not self.get_configuration('config_changeable'):
            self.log.debug('app %s config non changeable', self.current_app)
        else:
            self.log.debug("app %s mise à jour config", self.current_app)
            self.auth(0)
            self.check_desfire_call('change_key_settings', conf_mods)
            self.cur_configs[self.current_app].update(conf_mods)
            
        # Cas particulier PICC
        if self.current_app == 0 and self.config.get_setting(0, 'randomuid') and self.random_uid == False:
            self.auth(0)
            self.log.info('activation random UID')
            self.check_desfire_call('set_configuration', enable_randId=True)

    ### Gestion des fichiers
    def list_files(self):
        """ Retourne la liste des fichiers de l'application courrante

        Si il est impossible d'obtenir les données de depuis la carte
        les données du fichier de configuration sont retournées.
        """
        if self.current_app == 0:
            raise UsageError
        if not self.get_configuration('free_list'):
            try:
                self.auth(0)
            except AuthError:
                return self.config.list_files(self.current_app)
        
        return self.check_desfire_call('get_file_ids')
            
    def get_file_config(self, file_id):
        """ Retourne le paramétrage d'un fichier de l'application courrante

        Si il est impossible d'obtenir les données de depuis la carte
        les données du fichier de configuration sont retournées.

        @param : file_id : identifiant du fichier
        """
        if self.current_app == 0:
            raise UsageError
        if self.current_app not in self.files:
            self.files[self.current_app] = {}

        if file_id not in self.files[self.current_app]:
            try:
                self.auth(0)
            except AuthError:
                return self.config.get_file(self.current_app, file_id)
            else:
                self.files[self.current_app][file_id] = self.check_desfire_call('get_file_settings', file_id)

        return self.files[self.current_app][file_id]

    def update_file_config(self, file_id):
        """ Mise à jour du paramétrage d'un fichier de l'application courrante

        @param : file_id : identifiant du fichier
        """
        if self.current_app == 0:
            raise UsageError
        current_config = self.get_file_config(file_id)
        file_config = self.config.get_file(self.current_app, file_id)
        mods = {}
        for key in current_config:
            if key in file_config and current_config[key] != file_config[key]:
                if 'key' in key:
                    self.log.info('fichier %i config %s : %s → %s', file_id, key, current_config[key], file_config[key])
                    mods[key] = file_config[key]
                else:
                    self.log.info('fichier %i %s : %s au lieu de %s', file_id, key, current_config[key], file_config[key])

        if mods:
            self.check_desfire_call('change_file_settings', file_id, file_config)
            current_config.update(mods)

    def create_file(self, file_id):
        """ Creátion d'une fichier en fonction de la configuration

        @param file_id : identifiant du fichier
        """
        if self.current_app == 0:
            raise UsageError

        config = self.config.get_file(self.current_app, file_id)
        self.auth(0)
        self.check_desfire_call('create_file', file_id, **config)
        
    ### Méthodes de mise en conformité de la carte
    def update_application(self, app_id):
        """ Mise à jour ou création d'une application : configuration, clefs, fichiers

        @param app_id : identifiant de l'application
        """
        try:
            self.select_application(app_id)
        except CardApplicationNotFound:
            # L'application n'existe pas
            self.create_application(app_id)
        else:
            # Essais d'authentification avec les clefs de la config
            keys_ids = self.config.list_keys(app_id)
            # La clef maitre est mise en dernier afin de bénéficier de son
            # authentification pour la suite
            keys_ids.append(keys_ids.pop(0))
            for key_id in keys_ids:
                try:
                    self.auth(key_id)
                except AuthError:
                    continue

        # Mises à jour config et clefs
        self.update_configuration()
        self.update_keys()

        # Fichiers
        if app_id != 0:
            file_ids = self.list_files()
            for file_id in self.config.list_files(self.current_app):
                if file_id in file_ids:
                    self.update_file_config(file_id)
                else:
                    self.create_file(file_id)

    ### Méthodes de haut niveau pour gestion courrante
    def update_card(self):
        """ Mise à jour de la structure et de la configuration de la carte """
        for app_id in sorted(self.config.list_applications(managed=True)):
            self.update_application(app_id)
                
    def read_file(self, app_id, file_id):
        """ Lecture du contenu d'un fichier

        @param app_id  : identifiant de l'application
        @param file_id : identifiant du fichier à lire

        @return contenu du fichier
        """
        
        self.select_application(app_id)
        config = self.get_file_config(file_id)
        if config['ftype'] not in ( 'std', 'backup', 'linear_record', 'cyclic_record' ):
            # Pas encore compatible
            return NotImplemented
        
        keys = { config['r_key'], config['rw_key'] }
        if 14 not in keys:
            self.auth_select( keys )

        if config['ftype'] in ( 'linear_record', 'cyclic_record' ):
            data = self.check_desfire_call('read_data', file_id, config['comm_mode'], 0, 0, config['size'])
        else:
            data = self.check_desfire_call('read_data', file_id, config['comm_mode'], 0, config['size'])
        pytype = self.config.get_file(self.current_app, file_id)['pytype']
        if pytype == 'int':
            result = 0
            for i in range(len(data)):
                result += data[len(data)-i-1] << (8*i)
            return result
        elif pytype == 'hex':
            return ''.join([ '{:02x}'.format(x) for x in data ])
        elif pytype == 'str':
            try:
                return ''.join([ chr(x) for x in data ])
            except UnicodeDecodeError:
                return b''.join([ chr(x) for x in data ])
            
        return data

    def write_file(self, app_id, file_id, data):
        """ Ecriture dans un fichier

        @param app_id  : identifiant de l'application
        @param file_id : identifiant du fichier à lire
        @param data    : données à écrire
        """
        self.select_application(app_id)
        config = self.config.get_file(self.current_app, file_id)
        self.auth_select( [ config['w_key'], config['rw_key'] ] )

        if config['pytype'] == 'int':
            if not isinstance(data, int):
                try:
                    # Cas éventuel python2
                    if not isinstance(data, long):
                        raise ValueError('data must be an int or a long')
                except NameError:
                    # Pas de long en python3
                    raise ValueError('data must be an integer')
            data = toBytes('{:0{size}x}'.format(data, size=config['size']*2))
            
        elif config['pytype'] == 'hex':
            try:
                data = toBytes(data)
            except:
                raise ValueError('data must be a hex string')

        elif config['pytype'] == 'str':
            if not isinstance(data, str):
                raise ValueError('data must be a string')
            data = [ ord(char) for char in data ]
            
        self.check_desfire_call('write_data', file_id, config['comm_mode'], data)

    def format_PICC(self):
        """ Effacement de toute la carte """
        self.select_application(0)
        self.auth(0)
        self.check_desfire_call('format_PICC')
        # Reinit état courrant
        self.card_keys = { 0 : {} }
        self.cur_configs = {}
        self.files = {}
        self.auth_key_id = None

    def read_all(self):
        """ Lis tous les fichiers définis dans la configuration

        @return : { app_id : { file_id : <contenu> } }
        """
        data = {}
        for app_id in sorted(self.config.list_applications()):
            for file_id in self.config.list_files(app_id):
                try:
                    result = self.read_file(app_id, file_id)
                except CardError:
                    self.log.exception('app %i fichier %s impossible à lire', app_id, file_id)
                else:
                    if app_id not in data:
                        data[app_id] = {}
                    data[app_id][file_id] = result

        return data
