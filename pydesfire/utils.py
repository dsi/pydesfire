# -*- coding: utf-8 -*-

"""
Reprise des fonctions de smartcard.util avec petites adaptations pour usage
avec python3
"""

import binascii

def toASCIIString(hexlist):
    return binascii.unhexlify(''.join([ '{:02x}'.format(x) for x in hexlist ]))

def BinStringToHexList(binstr):
    if isinstance(binstr, str):
        return [ ord(x) for x in binstr ]
    else:
        r = binascii.hexlify(binstr)
        return [ int(r[i:i+2], 16) for i in range(0, len(r), 2) ]

def toBytes(str_repr):
    str_repr = str_repr.replace(' ', '')
    return [ int(str_repr[i:i+2], 16) for i in range(0, len(str_repr), 2) ]
