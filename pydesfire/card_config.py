# -*- coding: utf-8 -*-

"""
Gestion de la configuration d'une carte

© Frédéric Pauget <frederic.pauget@telecom-paristech.fr>

Ce logiciel est régi par la licence CeCILL, voir le fichier LICENSE.

"""

import re

try:
    # Python3
    import configparser
except ImportError:
    # Python2
    import ConfigParser as configparser

from .utils import toBytes
from .desfire_keys import DESFire_AES_key, DESFire_DES_key

class BadConfig(configparser.Error):
    pass

class CardConfig(object):
    cfg_key_re = re.compile('^(k|f)(\d{1,2})(?:\.(.*)|)$')
    ver_key_re = re.compile('^v(\d{1,3})$')

    # Structure interne de stockage:
    #  config = {  app_id :  { 'keys'   : { key_no : [ DESFire_key, DESFire_key2, .... ] },
    #                          'files'  : { file_no : [ type, options de création ] },
    #                          'config' : { key : val } }
    
    def __init__(self, filename):
        self._config = {}

        config = configparser.RawConfigParser()
        if config.read(filename) != [ filename ]:
            raise BadConfig('error on {}'.format(filename))
        
        for section in config.sections():
            if section[:2] == '0x':
                app_id = int(section[2:], 16)
            else:
                app_id = int(section)

            if app_id not in self._config:
                self._config[app_id] = self._app_config_dict(app_id)

            if app_id != 0:
                app_key_type = ',' + config.get(section, 'key_type')
            else:
                app_key_type = ''
                
            for key, value in config.items(section):
                cfg_key_match = self.cfg_key_re.match(key)
                if cfg_key_match:
                    groups = cfg_key_match.groups()
                    item_nb = int(groups[1])
                    if groups[0] == 'k':
                        self.configure_addkey(app_id, item_nb, '%s%s' % (value, app_key_type), groups[2] or key)
                    elif groups[0] == 'f':
                        self.configure_file(app_id, item_nb, value)

            for key in ( 'master_changeable', 'free_list', 'free_create_delete', 'config_changeable', 'randomuid' ):
                try:
                    self._config[app_id]['config'][key] = config.getboolean(section, key)
                except configparser.NoOptionError:
                    pass
                
            for key in ( 'nb_keys', 'change_key_no'):
                try:
                    self._config[app_id]['config'][key] = config.getint(section, key)
                except configparser.NoOptionError:
                    pass

            try:
                self._config[app_id]['config']['key_type'] = config.get(section, 'key_type')
            except configparser.NoOptionError:
                pass
                
            try:
                self._config[app_id]['managed'] = config.getboolean(section, 'managed')
            except configparser.NoOptionError:
                self._config[app_id]['managed'] = True

        self.default_key = {}
                    
    def _app_config_dict(self, app_id):
        """ Récupération du dictionnaire de configuration de l'application

        @param app_id  : id de l'application
        @return        : dictionnaire de configuration
        """
        return self._config.get(app_id, { 'keys' : {}, 'files' : {}, 'config' : {}, 'managed' : False })

    def configure_addkey(self, app_id, key_id, value, comment=''):
        """ Ajoute une clef à la configuration 

        @param app_id : id de l'application
        @param key_id : numéro du slot de clef
        @param value  : valeur de configuration : "valeur_clef_hexa,option1,option2,...."
                  options possibles : type de clef      : AES ou DES
                                      diversifification : d
                                      version de clef   : vX (pour AES)
        """
        key_version = None
        key_divert = False
        value = value.split(',')
        key_value = toBytes(value[0])
        for opt in value[1:]:
            opt = opt.strip()
            if opt == 'd':
                key_divert = True
            elif opt == 'AES':
                key_type = 'AES'
            elif opt == 'DES':
                key_type = 'DES'
            else:
                ver_key_match = self.ver_key_re.match(opt)
                if ver_key_match:
                    key_version = int(ver_key_match.groups()[0])

        if key_type == 'AES':
            key_class = DESFire_AES_key
        elif key_type == 'DES':
            key_class = DESFire_DES_key

        if key_id not in self._config[app_id]['keys']:
            self._config[app_id]['keys'][key_id] = []
        self._config[app_id]['keys'][key_id].append(key_class(key_value, key_version, key_divert, comment))

    def configure_file(self, app_id, file_id, value):
        """ Défini un fichier dans la configuration

        @param app_id  : id de l'application
        @param file_id : numéro du slot de clef
        @param value   : valeur de configuration : "valeur_clef_hexa,option1,option2,...."
                  options possibles : type de clef      : AES ou DES
                                      diversifification : d
                                      version de clef   : vX (pour AES)
        """
        value = value.strip().split(' ')
        config = {'ftype'     : value[0],
                  'comm_mode' : value[1],
                  'r_key'     : int(value[2]),
                  'w_key'     : int(value[3]),
                  'rw_key'    : int(value[4]),
                  'm_key'     : int(value[5])
        }
        
        if config['ftype'] in ( 'std', 'backup' ):
            config['size'] = int(value[6])
            config['pytype'] = value[-1]
        elif config['ftype'] in ( 'linear_record', 'cyclic_record' ):
            config['size'] = int(value[6])
            config['nb'] = int(value[7])
            config['pytype'] = value[-1]
        elif config['ftype'] == 'value':
            config['min'] = int(value[6])
            config['max'] = int(value[7])
            config['value'] = int(value[8])
            config['limited_credit'] = bool(value[9])
            config['pytype'] = 'int'
        else:
            raise ValueError('Bad file_type %s' % config['ftype'])
        
        self._config[app_id]['files'][file_id] = config

    def get_default_key(self, key_type):
        """ Retrourne la clef par défaut 

        @param key_type : type de clef : DES, 2K3DES, 3K3DES, AES
        """
        if key_type in self.default_key:
            return self.default_key[key_type]
        
        default = self._app_config_dict(0)['config'].get('default_key')
        if default:
            default = default.split(',')
            key_value = toBytes(default[0])
            ver_key_match = self.ver_key_re.match(default[-1])
            if ver_key_match:
                key_version = int(ver_key_match.groups()[0])
            else:
                key_version = 0
        else:
            key_value = [ 0 ] * 24
            key_version = 0

        if key_type == 'AES':
            key = DESFire_AES_key( key_value[:16], comment="defaut", version=key_version)
        elif key_type in ( 'DES', '2K3DES' ):
            key = DESFire_DES_key( key_value[:16], comment="defaut")
        elif key_type == '3K3DES':
            key = DESFire_DES_key( key_value[:24], comment="defaut")
        else:
            raise KeyError("key_type invalid")

        self.default_key[key_type] = key
        return key
        
    def get_setting(self, app_id, key, default=None):
        """ Récupération de la configuration de l'application

        @param app_id  : id de l'application
        @param key     : clef de configuration à récupérer
        @param default : valeur par défaut si la clef n'existe pas
        @return        : dictionnaire de configuration
        """
        val = self._app_config_dict(app_id)['config'].get(key, default)
        return val

    def list_keys(self, app_id):
        """ Liste des id de clefs de l'application

        @param app_id  : id de l'application
        @return        : liste des id de clefs
        """
        return sorted(self._app_config_dict(app_id)['keys'].keys())

    def get_keys(self, app_id, key_id, version=None):
        """ Récupération d'une liste de clefs

        @param app_id  : id de l'application
        @param key_id  : numéro du slot de clef
        @param version : ne retourne que les clefs ayant la version donnée
        @return        : liste de clefs (instances de DESFire_key)
        """
        keys = self._app_config_dict(app_id)['keys'].get(key_id, [])
        if app_id > 0:
            keys = [ self.get_default_key(self._app_config_dict(app_id)['config']['key_type']) ] + keys
        else:
            keys = [ self.get_default_key('DES') ] + keys

        if version != None:
            keys = [ key for key in keys if key.version == version ]

        if not keys:
            if version:
                raise KeyError('No key %i version %i for app %i' % (key_id, version, app_id))
            else:
                raise KeyError('No key %i for app %i' % (key_id, app_id))
        
        return keys

    def list_files(self, app_id):
        """ Liste des id de clefs de l'application

        @param app_id  : id de l'application
        @return        : liste des id des fichiers
        """
        return sorted(self._app_config_dict(app_id)['files'].keys())

    def get_file(self, app_id, file_id):
        """ Récupération de la configuration du fichier

        @param app_id  : id de l'application
        @return        : dictionnaire de configuration
        """
        return self._app_config_dict(app_id)['files'].get(file_id)

    def list_applications(self, managed=False):
        """ Liste des applications configurées

        @param managed : si True limite la liste aux applications gérées
        @return : liste des identifants des applications
        """
        if managed:
            return [ key for key in self._config if self._config[key]['managed'] ]
        else:
            return list(self._config.keys())
