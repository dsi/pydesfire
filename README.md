# Installation

  * Installer les dépendances :
    * requises :
      * `pyscard` https://pyscard.sourceforge.io/
      * `pycryptodome` https://pypi.org/project/pycryptodome/
    * facultatif, uniquement pour le shell : `ipython`

  * Il faut que le système ait un lecteur de smartcard et le driver correspondant.

# Démarrage rapide

1. éditer le fichier de configuration pour répondre aux besoins
1. placer une carte sur un lecteur
1. exécuter le shell de démo :

```
./shell.py sample_conf.ini
config=CardConfig("sample_conf.ini")
Starting job # 0 in a separate thread.

No card [1]:
============================ Initialisation carte ============================
card=DesFireCard(config, DesFireEV1(cardservice.connection))
DesFireCard app 0 auth None [1]:

DesFireCard app 0 auth None [1]: card.update_card()
DEBUG - UID non lu (mode random UID actif)
DEBUG - app 0 clef 0 : auth OK clef defaut (DES v0)
DEBUG - UID [4, 34, 71, 82, 166, 48, 128] (mode random UID actif)
DEBUG - app 0 config OK
INFO - app 0 clef 0 : MAJ vers clef 2016 (AES v1)
INFO - app 87148 : création, 2 clefs AES
DEBUG - app 87148 clef 0 : auth OK clef defaut (AES v0)
INFO - app 87148 config free_list à changer : True → False
DEBUG - app 87148 mise à jour config
INFO - app 87148 clef 1 : MAJ vers clef k1 (AES v0)
INFO - app 87148 clef 0 : MAJ vers clef k0 (AES v0)
DEBUG - app 87148 clef 0 : auth OK clef k0 (AES v0)

DesFireCard app 87148 auth 0 [2]: card.write_file(87148, 1, 478614)

DesFireCard app 87148 auth 0 [3]: card.read_file(87148, 1)
                          Out[3]: 478614

```
  * Commandes utiles :
    * `card.read_all()`

# Format du fichier de configuration

## Format général

Le fichier de contient une section par application du type :
```ConfigFile
[identifiant de application]
option1: valeur
option2: valeur
...
```

L'identifiant de l'application est soit exprimé en décimal soit en hexadécimal : par exemple ```[87148]``` et ```[0x1546c]``` identifient la même application.

L'application 0 est spécifique, elle correspond à la config PICC.

## Options possibles

| option              | valeur   | commentaire      |
|---------------------|----------|------------------|
| managed             | booléen  | défini si l'application est gérée ou non, cf comportement
| free_list           | booléen  | 
| free_create_delete  | booléen  |
| config_changeable   | booléen  |
| master_changeable   | booléen  |
| randomUID           | booléen  | uniquement PICC
| change_key_no       | entier   | hors PICC
| nb_keys             | entier   | hors PICC, valeur requise si application gérée
| key_type            | DES, 2K3DES, 3K3DES ou AES | hors PICC, valeur requise
| kX.description      | définition des clefs 
| fX                  | définition des fichiers

## Définition des clefs

Les clefs sont définies dans les options ```kX``` où X est le numéro de la clef. Le nom de l'option peut être suivi d'une description séparée par un point : ```kX.description```. Dans ce cas il est possible d'avoir plusieurs clefs pour un même numéro, cela est utile si on a plusieurs générations de cartes et des évolutions de clefs. L'authentification sera tentée avec toutes les clefs possibles. La dernière clef définie dans le fichier pour un numéro sera celle mise en place lors des mises à jour de la carte.

La valeur de la clef doit être fournie sous forme hexadécimale. Elle est suivi par les options de clefs. Les options possibles sont :
 * ```d``` : indique que la clef doit être diversifiée
 * ```vX``` : indique la version de clef pour les clefs AES
 * ```AES``` ou ```DES``` : uniquement dans la config PICC : indique le type de clef, les variations de DES (2k3 ou 2k3) sont automatiquement prises en compte en fonction de la longueur.

Les clefs par défaut sont automatiquement ajoutées si besoin.

## Définition des fichiers

Les fichiers sont définis dans les options ```fX``` où X est l'identifiant du fichier. La valeur contient plusieurs éléments séparés par des espaces :
 * type de fichier : 
   * ```std``` : stockage simple de valeur
   * autres types à implémenter
 * type de communication : ```PLAIN```, ```MAC``` ou ```ENCRYPT```
 * numéro de la clef de lecture
 * numéro de la clef d'écriture
 * numéro de la clef de lecture + écriture
 * numéro de la clef de changement de droits
 * pour le type std la taille du fichier en octets
 * type de donnée (conversions à l'écriture et à la lecture) :
  * ```hex``` : chaines représentant une valeur hexadécimale, le nombre de digits hexa est double de la taille
  * ```int``` : entier, la valeur maximale sera égale à 2^(taille×8)
  * ```str``` : chaine de caractère, le nombre de caractères est égal à la taille

## Comportement

Pour les application gérées (par défaut à moins que managed soit faux) la méthode ```DesFireCard.update_card``` aura pour comportement de :
 * créer les applications et fichiers inexistants
 * mettre en conformité la configuration de la carte avec les valeurs du fichier
 * mettre les clefs à la dernière valeur indiquée dans le fichier

Pour les options non présentes dans le fichier le paramétrage par défaut sera conservé.

# Documentation

Les classes sont documentées via les docstring python.
